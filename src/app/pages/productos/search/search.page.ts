import { Component, OnInit } from '@angular/core';
import { AppProdutsQueryFilter } from '../../../interfaces/app-produts-query-filter';
import { AppProductsGetDto } from '../../../models/app-products-get-dto';
import { IUsuario } from '../../../interfaces/iusuario';
import { ProductoService } from '../../../services/producto.service';
import { GeneralService } from '../../../services/general.service';
import { ToastController, ActionSheetController, AlertController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MunicipioGetDto } from '../../../models/municipio-get-dto';
import { BuscadorMunicipioComponent } from '../../../components/buscador-municipio/buscador-municipio.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  appProdutsQueryFilter: AppProdutsQueryFilter;
  appProductsGetDto: AppProductsGetDto[] = [];
  appProductsGetDtoNew: AppProductsGetDto[] = [];
  itemMunicipioGetDto: MunicipioGetDto;

  usuario: IUsuario;
  tituloVentaEdicion: string;
  public showLoading: boolean = false;
  form: FormGroup;
  constructor(private productoService: ProductoService,
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public generalService: GeneralService,
    public toastController: ToastController,
    private router: Router,
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,) {
    this.usuario = this.generalService.GetUsuario();
    this.buildForm();
  }

  ngOnInit() {

    this.productoService.allProducts$.subscribe(allProducts => {
      this.appProductsGetDto = allProducts.data;
      console.log('productos al cargar busqueda', this.appProductsGetDto)

    });

    this.appProdutsQueryFilter = {
      pageSize: 20,
      pageNumber: 1,
      id: 0,
      code: '',
      description1: '',
      description2: '',
      searchText: ''

    }

    this.refresh();

  }

  onChangeSearchText(event) {

    this.appProdutsQueryFilter.searchText = event.target.value;
    this.refresh();

  }


  refresh() {
    this.showLoading = true;
    this.productoService.GetAllAppProducts(this.appProdutsQueryFilter);




    this.showLoading = false;
  }

  async onBuscarMunicipio() {

    const modal = await this.modalCtrl.create({
      component: BuscadorMunicipioComponent,
      componentProps: {
        userConectado: this.usuario.user
      }
    });

    await modal.present();

    const { data } = await modal.onDidDismiss();
    console.log("datos retornados por el modal*******", data);
    this.itemMunicipioGetDto = data.itemMunicipio;
    this.form.get('idMunicipio').setValue(data.itemMunicipio.recnum);
    this.form.get('descripcionMunicipio').setValue(data.itemMunicipio.descMunicipio);


  }
  save(event) { }
  private buildForm() {

    this.form = this.formBuilder.group({

      idMunicipio: [0, []],
      descripcionMunicipio: ['', []],
    });
  }
}
